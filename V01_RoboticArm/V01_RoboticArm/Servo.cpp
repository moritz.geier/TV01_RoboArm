#include "Servo.h"

double Servo::_period = 0;
std::vector<Servo*> Servo::instances;
PCA9685 Servo::_driver;

Servo::Servo()
{

}

Servo::Servo(int nr, int startAngle, double minTime, double maxTime, int maxAngle)
{
	_servoNr = nr;
	_minCount = Servo::ConvertTimeToCount(minTime);
	_maxCount = Servo::ConvertTimeToCount(maxTime);
	_minTime = minTime;
	_maxTime = maxTime;
	_maxAngle = maxAngle;

	Servo::SetAngle(startAngle);
	std::cout << "Servo " << _servoNr << "\t[Ready]" << "\t\tSet to " << startAngle << "degrees" << std::endl;

	instances.push_back(this);
}

void Servo::SetFrequency(int freq)
{
	_driver.setPWMFreq(freq);
	_period = 1.0 / freq;
	for (std::vector<Servo*>::iterator it = instances.begin(); it != instances.end(); it++)
	{
		Servo* s = *it;
		s->_minCount = s->ConvertTimeToCount(s->_minTime);
		s->_maxCount = s->ConvertTimeToCount(s->_maxTime);
	}
}

void Servo::SelectDriver(int i2cChannel, int i2cAdr, int freq)
{
	_driver.init(i2cChannel, i2cAdr);
	Servo::SetFrequency(freq);
}

void Servo::SetAngle(double ang)
{
		Servo::SetPos(ang*(_maxCount - _minCount) / _maxAngle + _minCount);
}

void Servo::SetTimeWithOffset(double time)
{
		Servo::SetPos((time)*_maxCount / (_maxTime));
}

void Servo::SetTimeWithoutOffset(double time)
{
		Servo::SetPos((time)*_maxCount / (_maxTime)+_minCount);
}

void Servo::SetPos(int pos)
{
	if (_currentPos != pos)
	{
		if (pos <= _minCount || pos >= _maxCount)
		{
			std::cout << "Position of Servo " << _servoNr << " can't be reached" << std::endl;
		}
		else
		{
			std::cout << "Set to Position " << pos << " \tMax = " << _maxCount << " \tMin = " << _minCount << std::endl;
			_driver.setPWM(_servoNr, pos);
			_currentPos = pos;
		}
	}
	else
	{
		std::cout << "Servo " << _servoNr << " is already on " << pos << std::endl;
	}
}

void Servo::Shutdown()
{
	_driver.setPWM(_servoNr, 0);
	std::cout << "Servo " << _servoNr << " is now turned off" << std::endl;
}

int Servo::ConvertTimeToCount(double time)
{
	return RESOLUTION * time / _period;
}