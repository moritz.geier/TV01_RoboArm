#ifndef SERVO_H
#define SERVO_H

#define RESOLUTION 4096

#include <iostream>
#include <vector>
#include "PCA9685.h"


class Servo
{
public:
	Servo();
	Servo(int nr, int startPoint, double minTime, double maxTime, int maxAngle);
	static void SetFrequency(int freq);
	static void SelectDriver(int i2cChannel, int i2cAdr, int freq);
	void SetAngle(double ang);
	void SetTimeWithOffset(double time);
	void SetTimeWithoutOffset(double time);
	void SetPos(int pos);
	void Shutdown();


private:
	static double _period;
	static std::vector<Servo*> instances;
	static PCA9685 _driver;
	int _servoNr;
	int _minCount;
	double _minTime;
	int _maxCount;
	double _maxTime;
	double _maxAngle;

	int _currentPos;

	int ConvertTimeToCount(double time);
};
#endif