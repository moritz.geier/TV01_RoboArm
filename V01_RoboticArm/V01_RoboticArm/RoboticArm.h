#ifndef ROBOTIC_ARM_H
#define ROBOTIC_ARM_H

#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <vector>
#include "Servo.h"

#define MIN_COUNT		640
#define MAX_COUNT		3050
#define MIDDLE_COUNT	(MAX_COUNT-MIN_COUNT)/2 + MIN_COUNT
#define MAX_ANGLE		270
#define MIN_TIME		0.0005
#define MAX_TIME		0.0025
#define START_ANGLE		135
#define ARM_LENGTH		100

class RoboticArm
{
public:
	RoboticArm();
	void SetKoord(double x, double y);

private:
	Servo _s0;
	Servo _s1;
	Servo _s2;
};

#endif