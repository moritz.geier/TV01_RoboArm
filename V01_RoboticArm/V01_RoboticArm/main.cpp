#include <wiringPi.h>
#include <iostream>
#include "RoboticArm.h"

int main(void)
{
	wiringPiSetupSys();
	
	double x = 135, y = 135;
	
	std::cout << "Start Setup..." << std::endl;

	RoboticArm arm;
	
	std::cout << "Setup finished!" << std::endl;
	while (true)
	{
		std::cout << "x = ";
		std::cin >> x;
		std::cout << "y =";
		std::cin >> y;
		arm.SetKoord(x,y);
	}
	return 0;
}
